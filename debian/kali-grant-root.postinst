#!/bin/sh

set -e

. /usr/share/debconf/confmodule

log() {
    if [ -n "$DEBCONF_RECONFIGURE" ]; then
	echo "$1"
    fi
}

get_group_members() {
    local group="$1"
    getent group "$group" | cut -d: -f4 | sed -e 's/,/\n/'
}

is_group_member() {
    local group="$1"
    local user="$2"
    if get_group_members "$group" | grep -q ^"$user"$; then
	return 0
    fi
    return 1
}

configure_grant_root() {
    db_get kali-grant-root/policy || true
    what="$RET"

    case "$what" in
	enable)
	    for user in $(get_group_members sudo); do
		if ! is_group_member "kali-trusted" "$user"; then
		    echo "INFO: Adding user $user to kali-trusted group."
		    adduser "$user" "kali-trusted"
		fi
	    done
	    ;;
	disable)
	    for user in $(get_group_members kali-trusted); do
		echo "INFO: Removing user $user from kali-trusted group."
		deluser "$user" "kali-trusted"
	    done
	    ;;
	"no change")
	    log "INFO: No change to kali-grant-root's policy."
	    ;;
	*)
	    echo "WARNING: unexpected value in debconf's kali-grant-root/policy: '$what'" >&2
	    ;;
    esac
}

case "$1" in
    configure)
	addgroup --system kali-trusted 2>/dev/null || true
	configure_grant_root
	;;
esac

#DEBHELPER#
